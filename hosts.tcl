#! /usr/bin/env tclsh
# A library and command line tool for working with /etc/hosts.
# Copyright (c) 2018, 2021, 2024 D. Bohdan and contributors listed in AUTHORS.
# See the file LICENSE for details.

package require Tcl 8.5 9
if {[package vsatisfies [info patchlevel] 8.5] &&
    ![package vsatisfies [info patchlevel] 8.6]} {
    package require try
}

namespace eval hosts {
    variable version 0.7.0
}



### Parsing/serialization.

# Transform the raw text of /etc/hosts into a list of entry dicts.  Each entry
# dict is {?ip 1.2.3.4 hostnames {hostname1 hostname2 ...}? ?comment blah?}.
proc hosts::parse text {
    set entries {}

    foreach line [split $text \n] {
        set entry {}
        regexp {^([^#]*?)(\s*#.+)?$} $line _ decl comment
        if {$decl ne {}} {
            dict set entry ip [lindex $decl 0]
            dict set entry hostnames [lrange $decl 1 end]
        }
        if {$comment ne {}} {
            dict set entry comment $comment
        }
        lappend entries $entry
    }

    if {[lindex $entries end] eq {}} {
        set entries [lrange $entries 0 end-1]
    }

    return $entries
}

# Transform a list of entires in text suitable for writing to /etc/hosts.
proc hosts::serialize hosts {
    set lines {}
    foreach entry $hosts {
        set line {}
        if {[dict exists $entry ip]} {
            append line [dict get $entry ip]
            append line " [dict get $entry hostnames]"
        }
        if {[dict exists $entry comment]} {
            append line [dict get $entry comment]
        }
        lappend lines $line
    }
    return [join $lines \n]
}


### Primitives for manipulating hosts.

# Find the entries with the IP address and/or the hostname that match those in
# $args.  If neither is given, find all entries.  The return value is the list
# of either the entries themselves (-indices 0) or their positions in $hosts
# (-indices 1).  With -comments 0 and -empty 0 comment-only and empty entries
# respectively are filtered out.
proc hosts::find {hosts args} {
    named-args {
        -ip        {ip {}}
        -hostname  {hostname {}}
        -indices   {indices 0}
        -comments  {comments 1}
        -empty     {empty 1}
    }

    set found {}
    set i -1
    foreach entry $hosts {
        incr i
        if {$ip ne {} &&
            (![dict exists $entry ip] ||
             [dict get $entry ip] ne $ip)} continue
        if {$hostname ne {} &&
            (![dict exists $entry hostnames] ||
             $hostname ni [dict get $entry hostnames])} continue
        if {!$empty &&
            [llength $entry] == 0} continue
        if {!$comments &&
            ![dict exists $entry ip]} continue
        if {$indices} {
            lappend found $i
        } else {
            lappend found $entry
        }
    }
    return $found
}

# Add $hostname to the hostnames of $entry idempotently.
proc hosts::add-hostname {entry hostname} {
    if {$hostname ni [dict get $entry hostnames]} {
        dict lappend entry hostnames $hostname
    }
    return $entry
}

# Remove $hostname from the hostnames of $entry.  Change nothing if it isn't
# present.  Remove the keys "ip" and "hostnames" with the last hostname.
proc hosts::remove-hostname {entry hostname} {
    set updated [lsearch -all \
                         -inline \
                         -not \
                         [dict get $entry hostnames] \
                         $hostname]
    if {$updated eq {}} {
        dict unset entry ip
        dict unset entry hostnames
    } else {
        dict set entry hostnames $updated
    }
    return $entry
}



### Higher-level commands available from the command line.

namespace eval hosts::commands {
    variable ns [namespace parent]
    namespace path $ns
}

proc hosts::commands::add {hosts ip hostname} {
    if {[find $hosts -ip $ip -hostname $hostname -indices 1] ne {}} {
        return $hosts
    }

    set indices [find $hosts -ip $ip -indices 1]
    if {$indices eq {}} {
        lappend hosts [list ip $ip hostnames [list $hostname]]
    } else {
        # Allow repeating IPs.
        set index [lindex $indices 0]
        set entry [lindex $hosts $index]
        lset hosts $index [add-hostname $entry $hostname]
    }
    return $hosts
}

proc hosts::commands::replace-ip {hosts hostname new-ip} {
    set index [find $hosts -hostname $hostname \
                           -indices 1]
    if {[llength $index] > 1} {
        set error [list several entries have hostname $hostname]
        log error $error
        return -code error -errorcode {HOSTS ENTRY TOO_MANY} $error
    }

    set entry [lindex $hosts $index]
    # If the IP address has a single hostname associated with it, replace the IP
    # address in the entry to preserve the hostname's position in the file and
    # its comment.  If it has more, remove the hostname from the IP address and
    # add it separately with the new IP.
    if {[llength [dict get $entry hostnames]] == 1} {
        dict set entry ip ${new-ip}
        lset hosts $index $entry
        return $hosts
    } else {
        set ip [dict get [lindex $hosts $index] ip]
        set updated [rm $hosts $ip $hostname]
        set updated2 [add $updated ${new-ip} $hostname]
        return $updated2
    }
}

proc hosts::commands::rm {hosts ip hostname} {
    set findArgs [list -indices 1]
    if {$ip ne {*}} {
        lappend findArgs -ip $ip
    }
    if {$hostname ne {*}} {
        lappend findArgs -hostname $hostname
    }

    set indices [find $hosts {*}$findArgs]
    if {[find [pick $hosts $indices] -comments 0 -empty 0] eq {}} {
        set error [list no entry found with ip $ip and hostname $hostname]
        log error $error
        return -code error -errorcode {HOSTS ENTRY NOT_FOUND} $error
    }

    foreach index [lsort -dec $indices] {
        set entry [lindex $hosts $index]
        if {[dict exists $entry hostnames]} {
            set updated [remove-hostname $entry $hostname]
            if {$updated eq {}} {
                set hosts [lreplace $hosts $index $index]
            } else {
                lset hosts $index $updated
            }
        }
    }
    return $hosts
}

proc hosts::commands::version {} {
    variable ns
    puts stderr [set ${ns}::version]
}

proc hosts::commands::help {} {
    set withArgs {}
    set withoutArgs {}
    set options {[--log-level level] [--file path]}
    foreach command [::info commands [namespace current]::*] {
        set arguments [::info args $command]
        if {$arguments eq {}} {
            lappend withoutArgs [list [file tail $::argv0] \
                                      [namespace tail $command]]
        } else {
            lappend withArgs [list [file tail $::argv0] \
                             $options \
                             [namespace tail $command] \
                             {*}[lrange $arguments 1 end]]
        }
    }
    set prefix {usage: }
    foreach line [concat [lsort $withArgs] [lsort $withoutArgs]] {
        puts "$prefix[join $line]"
        regsub -all . $prefix { } prefix
    }
}



### Command line infrastructure.

namespace eval hosts::cli {
    variable ns [namespace parent]
    variable exitStatuses {
        bad-cli-arguments  100
        no-hosts-file      101
        bad-hosts-file     102
        read-error         103
        write-error        104
        nonexistent-entry  105
        too-many-entries   106
        other-error        200
    }
    namespace path $ns
}

proc hosts::cli::main {argv0 argv} {
    variable ns
    lassign [parse-argv $argv] hostsPath positional
    set arguments [lassign $positional command]

    set commandsFullPath [lsort [::info commands ${ns}::commands::*]]
    set commands {}
    foreach commandFullPath $commandsFullPath {
        lappend commands [namespace tail $commandFullPath]
    }

    if {$command eq {}} { set command help }
    if {$command ni $commands} {
        log error [list unknown command $command]
        log error [list possible commands are $commands]
        exit-with-status bad-cli-arguments
    }

    try {
        set commandArgs [info args commands::$command]
        set count [llength $arguments]
        set expected [llength $commandArgs]

        # Commands take either no arguments or hosts data followed by zero
        # or more user arguments.
        if {$expected == 0 && $count > 0} {
            log error [list command $command takes no arguments]
            exit-with-status bad-cli-arguments
        } elseif {$expected > 0 && $count != $expected - 1} {
            log error [list command $command takes \
                            arguments [lrange $commandArgs 1 end]]
            exit-with-status bad-cli-arguments
        }

        if {$expected == 0} {
            commands::$command
        } else {
            set hosts [parse-file $hostsPath]
            set updatedHosts [commands::$command $hosts {*}$arguments]
            try {
                set ch [open $hostsPath w]
                puts $ch [serialize $updatedHosts]
                close $ch
            } on error {_ opts} {
                log error [list can't write $hostsPath]
                log debug $opts
                exit-with-status write-error
            }
        }
    } trap {HOSTS ENTRY NOT_FOUND} e {
        exit-with-status nonexistent-entry
    } trap {HOSTS ENTRY TOO_MANY} e {
        exit-with-status too-many-entries
    } on error {e opts} {
        log error $e
        log debug [list internal error $opts]
        exit-with-status other-error
    }
}

proc hosts::cli::exit-with-status name {
    variable exitStatuses
    exit [dict get $exitStatuses $name]
}

proc hosts::cli::parse-file path {
    if {![file exists $path]} {
        log error [list $path does not exist]
        exit-with-status no-hosts-file
    }
    try {
        set ch [open $path r]
        set text [read $ch]
        close $ch
    } on error {_ opts} {
        log error [list can't read $path]
        log debug [list read error $opts]
        exit-with-status read-error
    }
    try {
        set hosts [parse $text]
    } on error {_ opts} {
        log error [list can't parse $path]
        log debug [list parse error $opts]
        exit-with-status bad-hosts-file
    }
    return $hosts
}

proc hosts::cli::parse-argv argv {
    for {set i 0} {$i < [llength $argv]} {incr i 2} {
        if {![string match -* [lindex $argv $i]]} break
    }
    set args [lrange $argv 0 $i-1]
    set command [lrange $argv $i end]
    log debug [list args $args command $command]

    set namedArgs {
        --log-level  {logLevelArg info}
        --file       {hostsPath /etc/hosts}
    }
    try {
        named-args $namedArgs
    } on error e {
        log error $e
        log error [list valid command line options are \
                        [list {*}$namedArgs]]
        exit-with-status bad-cli-arguments
    }

    try {
        set logLevels [set [namespace parent]::logLevels]
        set [namespace parent]::minLogLevel \
            [dict get $logLevels $logLevelArg]
    } trap {TCL LOOKUP DICT} e {
        log error [list unknown log level $logLevelArg]
        log error [list valid log levels are [dict keys $logLevels]]
        exit-with-status bad-cli-arguments
    }

    return [list $hostsPath $command]
}



### Helper procs.

namespace eval hosts {
    variable minLogLevel 2
    variable logLevels {
        trace    0
        debug    1
        info     2
        warn     3
        error    4
        quiet    99
    }
}

proc hosts::log {level message} {
    variable logLevels
    variable minLogLevel

    set levelNum [dict get $logLevels $level]
    if {$levelNum < $minLogLevel} return
    set timestamp [clock format [clock seconds] \
                                -format %Y-%m-%dT%H:%M:%S]
    puts stderr "$timestamp $level: $message"
}

proc hosts::named-args mapping {
    upvar 1 args args

    dict for {key dest} $mapping {
        catch {unset default}
        switch -exact -- [llength $dest] {
            1 { set varName $dest }
            2 { lassign $dest varName default }
            default { error [list expected {varName ?default?} but got $dest] }
        }

        upvar 1 $varName v
        if {[dict exists $args $key]} {
            set v [dict get $args $key]
        } elseif {[::info exists default]} {
            set v $default
        } else {
            error [list missing required argument $key]
        }
        dict unset args $key
    }
    if {$args ne {}} {
        error [list unknown extra arguments $args]
    }
}

proc hosts::pick {list indices} {
    set result {}
    foreach index $indices {
        lappend result [lindex $list $index]
    }
    return $result
}



# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    hosts::cli::main $argv0 $argv
}

package provide hosts $hosts::version
