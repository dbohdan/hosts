# hosts

`hosts` is a command line tool and a Tcl library for manipulating the contents
of `/etc/hosts`.


## Motivation

I needed to add and remove hosts in `/etc/hosts` programmatically.  I liked
<https://github.com/cbednarski/hostess>, but it didn't preserve arbitrary
comments.


## Requirements

* Either Tcl 8.6 or Tcl 8.5 with the package `try` to use the library and the CLI tool;
* Tcllib to run the tests.


## Installation for CLI use

1\. Install Tcl 8.6.  On Debian and Ubuntu run the command  `sudo apt install tcl`.  On Windows install [Magicsplat Tcl/Tk for Windows](http://www.magicsplat.com/tcl-installer/).

2\. On \*nix copy `hosts.tcl` to somewhere on your `$PATH`. For example:

```sh
sudo install hosts.tcl /usr/local/bin/hosts
```


## Usage

Any of this may change.

### Command line

* hosts [--log-level level] [--file path] add ip hostname
* hosts [--log-level level] [--file path] replace-ip hostname new-ip
* hosts [--log-level level] [--file path] rm ip hostname
* hosts help
* hosts version

The operations are idempotent.  Removing the last hostname for an IP address
removes the IP address.  Different errors produce different exit statuses.  See
the dictionary `exitStatuses` in `hosts::cli`.

### API

`hosts` is a list of entries.  An `entry` is the dictionary `{?ip 1.2.3.4
hostnames {hostname1 hostname2 ...}? ?comment blah?}`.  An entry can not have
only the key `ip` or only the key `hostname`; it must have both or neither.
An `index` is an integer &ge; 0 that indicates the position of an `entry` in a
`hosts` list.  `text` is the raw contents of `/etc/hosts`.

* `hosts::parse text` → `hosts`
* `hosts::serialize hosts` → `text`
* `hosts::find hosts ?-indices 0? ?-ip 1.2.3.4? ?-hostname foo? ?-comments 1? ?-empty 1?` → `hosts`
* `hosts::find hosts -indices 1 ?-ip 1.2.3.4? ?-hostname foo? ?-comments 1? ?-empty 1?` → 
  `{index1 index2 ...}`
* `hosts::add-hostname entry hostname` → `entry`
* `hosts::remove-hostname entry hostname` → `entry`
* `hosts::commands::add hosts ip hostname` → `hosts`
* `hosts::commands::replace-ip hosts hostname new-ip` → `hosts`
* `hosts::commands::rm hosts ip hostname` → `hosts`


## License

MIT.  See the file `LICENSE`.
