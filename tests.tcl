#! /usr/bin/env tclsh
# Copyright (c) 2018, 2021 D. Bohdan and contributors listed in AUTHORS.
# See the file LICENSE for details.

package require fileutil
package require tcltest
package require textutil

set path [file dirname [file dirname [file normalize $argv0/___]]]
cd $path
lappend auto_path $path
set tclsh [info nameofexecutable]

package require hosts
set hosts::minLogLevel 2


set hosts [hosts::parse [textutil::undent [string range {
    127.0.0.1 localhost
    127.0.1.1 foo
    192.168.1.1 netbar      # comment
    # this is a comment line
    ::1 localhost ip6-localhost ip6-loopback
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
        # another comment line
} 1 end]]]

tcltest::customMatch list {apply {{xs ys} {
    foreach x $xs y $ys {
        if {$x != $y} { return 0 }
    }
    return 1
}}}


tcltest::test find-1.1 {} -body {
    hosts::find $hosts -indices 1
} -result {0 1 2 3 4 5 6 7}

tcltest::test find-1.2 {} -body {
    hosts::find $hosts -ip 127.0.0.1 \
                       -indices 1
} -result 0

tcltest::test find-1.3 {} -body {
    hosts::find $hosts -hostname localhost \
                       -indices 1
} -result {0 4}

tcltest::test find-1.4 {} -body {
    hosts::find $hosts -ip 127.0.0.1 \
                       -hostname localhost \
                       -indices 1
} -result 0

tcltest::test find-1.5 {} -body {
    hosts::find $hosts -comments 0 \
                       -indices 1
} -result {0 1 2 4 5 6}

tcltest::test find-1.5 {} -body {
    hosts::find {
        {ip 127.0.0.1 hostnames foo}
        {}
        {}
        {}
        {ip 127.0.0.1 hostnames bar}
        {}
    } -empty 0 -indices 1
} -result {0 4}


tcltest::test parse-1.1 {} -body {
    hosts::parse "192.168.1.1 hello\n192.168.1.2 world"
} -match list -result {
    {ip 192.168.1.1 hostnames hello}
    {ip 192.168.1.2 hostnames world}
}

tcltest::test parse-1.2 {} -body {
    hosts::parse "192.168.1.1 hello\n# comment\n192.168.1.2 world   #another"
} -match list -result {
    {ip 192.168.1.1 hostnames hello}
    {comment {# comment}}
    {ip 192.168.1.2 hostnames world comment {   #another}}
}

tcltest::test parse-1.3 {} -body {
    hosts::parse \n\n\n\n
} -match list -result {{} {} {}}


tcltest::test serialize-1.1 {} -body {
    hosts::serialize {
        {ip 192.168.1.1 hostnames hello}
        {comment {# comment}}
        {ip 192.168.1.2 hostnames world comment {   #another}}
    }
} -result "192.168.1.1 hello\n# comment\n192.168.1.2 world   #another"


tcltest::test add-hostname-1.1 {} -body {
    hosts::add-hostname {ip 127.0.0.1 hostnames localhost} \
                        sprawl
} -result {ip 127.0.0.1 hostnames {localhost sprawl}}

tcltest::test add-hostname-1.2 {} -body {
    hosts::add-hostname {ip 127.0.0.1 hostnames {localhost sprawl}} \
                        sprawl
} -result {ip 127.0.0.1 hostnames {localhost sprawl}}


tcltest::test remove-hostname-1.1 {} -body {
    hosts::remove-hostname {ip 127.0.0.1 hostnames {localhost sprawl}} \
                           sprawl
} -result {ip 127.0.0.1 hostnames localhost}

tcltest::test remove-hostname-1.2 {} -body {
    hosts::remove-hostname {ip 127.0.0.1 hostnames localhost} \
                           localhost
} -result {}

tcltest::test remove-hostname-1.3 {} -body {
    hosts::remove-hostname {ip 127.0.0.1
                            hostnames localhost
                            comment { # argh}} \
                           localhost
} -result {comment { # argh}}


tcltest::test pick-1.1 {} -body {
    hosts::pick {a b c d e f} {1 0 5 0}
} -result {b a f a}


tcltest::test commands-1.1 add -cleanup {unset updated} -body {
    set updated [hosts::commands::add $hosts 127.0.0.1 first]
    hosts::commands::add $updated 127.0.0.1 second
} -match list -result {
    {ip 127.0.0.1 hostnames {localhost first second}}
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-localhost ip6-loopback}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}

tcltest::test commands-1.2 add -body {
    hosts::commands::add $hosts 172.16.1.1 hello
} -match list -result {
    {ip 127.0.0.1 hostnames localhost}
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-localhost ip6-loopback}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
    {ip 172.16.1.1 hostnames hello}
}

tcltest::test commands-1.3 add -body {
    hosts::commands::add {
        {ip 192.168.1.1 hostnames foo}
        {ip 192.168.1.2 hostnames baz}
        {ip 192.168.1.1 hostnames bar}
    } 192.168.1.1 qux
} -match list -result {
    {ip 192.168.1.1 hostnames {foo qux}}
    {ip 192.168.1.2 hostnames baz}
    {ip 192.168.1.1 hostnames bar}
}

tcltest::test commands-1.4 add -body {
    hosts::commands::add {
        {ip 192.168.1.1 hostnames foo}
        {ip 192.168.1.2 hostnames baz}
        {ip 192.168.1.1 hostnames bar}
    } 192.168.1.1 bar
} -match list -result {
    {ip 192.168.1.1 hostnames foo}
    {ip 192.168.1.2 hostnames baz}
    {ip 192.168.1.1 hostnames bar}
}

tcltest::test commands-1.5 add -body {
    hosts::commands::add {
        {ip 192.168.1.1 hostnames {foo bar}}
        {ip 192.168.1.2 hostnames baz}
        {ip 192.168.1.1 hostnames bar}
    } 192.168.1.1 bar
} -match list -result {
    {ip 192.168.1.1 hostnames {foo bar}}
    {ip 192.168.1.2 hostnames baz}
    {ip 192.168.1.1 hostnames bar}
}


tcltest::test commands-2.1 rm -body {
    hosts::commands::rm $hosts ::1 ip6-loopback
} -match list -result {
    {ip 127.0.0.1 hostnames localhost}
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-localhost}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}

tcltest::test commands-2.2 rm -body {
    hosts::commands::rm $hosts ::1 ip6-localhost
} -match list -result {
    {ip 127.0.0.1 hostnames localhost}
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-loopback}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}

tcltest::test commands-2.3 rm -body {
    hosts::commands::rm $hosts 127.0.1.1 foo
} -match list -result {
    {ip 127.0.0.1 hostnames localhost}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-localhost ip6-loopback}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}

tcltest::test commands-2.4 rm -body {
    hosts::commands::rm $hosts * localhost
} -match list -result {
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {ip6-localhost ip6-loopback}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}

tcltest::test commands-2.5 rm -body {
    hosts::commands::rm $hosts ff02::1 *
} -match list -result {
    {ip 127.0.0.1 hostnames localhost}
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.1.1 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-localhost ip6-loopback}}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}

tcltest::test commands-2.6 rm -body {
    hosts::commands::rm $hosts * *
} -match list -result {
    {comment {      # comment}}
    {comment {# this is a comment line}}
    {comment {    # another comment line}}
}

tcltest::test commands-2.7 rm -body {
    hosts::commands::rm {
        {ip 192.168.1.1 hostnames {foo bar}}
        {ip 192.168.1.2 hostnames baz}
        {ip 192.168.1.1 hostnames bar}
    } 192.168.1.1 bar
} -match list -result {
    {ip 192.168.1.1 hostnames foo}
    {ip 192.168.1.2 hostnames baz}
}

tcltest::test commands-3.1 replace-ip -body {
    hosts::commands::replace-ip {
        {ip 192.168.1.1 hostnames {foo bar}}
        {ip 192.168.1.2 hostnames baz}
    } baz 192.168.1.3
} -match list -result {
    {ip 192.168.1.1 hostnames {foo bar}}
    {ip 192.168.1.3 hostnames baz}
}

tcltest::test commands-3.2 replace-ip -body {
    hosts::commands::replace-ip {
        {ip 192.168.1.1 hostnames {foo bar}}
        {ip 192.168.1.2 hostnames baz}
    } foo 192.168.1.3
} -match list -result {
    {ip 192.168.1.1 hostnames bar}
    {ip 192.168.1.2 hostnames baz}
    {ip 192.168.1.3 hostnames foo}
}

tcltest::test commands-3.3 replace-ip -body {
    hosts::commands::replace-ip $hosts netbar 192.168.7.10
} -match list -result {
    {ip 127.0.0.1 hostnames localhost}
    {ip 127.0.1.1 hostnames foo}
    {ip 192.168.7.10 hostnames netbar comment {      # comment}}
    {comment {# this is a comment line}}
    {ip ::1 hostnames {localhost ip6-localhost ip6-loopback}}
    {ip ff02::1 hostnames ip6-allnodes}
    {ip ff02::2 hostnames ip6-allrouters}
    {comment {    # another comment line}}
}


tcltest::test cli-1.1 version -body {
    exec $tclsh [file join $path hosts.tcl] version 2>@1
} -result $hosts::version

tcltest::test cli-2.1 help -body {
    exec $tclsh [file join $path hosts.tcl] help 2>@1
} -match glob -result {usage: hosts.tcl*}

tcltest::test cli-3.1 add -body {
    set file [tcltest::makeFile {192.168.1.1 foo} hosts.test]
    exec $tclsh \
         [file join $path hosts.tcl] \
         --file $file \
         add 127.0.0.1 localhost
    fileutil::cat $file
} -result "192.168.1.1 foo\n127.0.0.1 localhost\n" -cleanup {
    unset file
}

tcltest::test cli-4.1 rm -body {
    set file [tcltest::makeFile {192.168.1.1 foo bar} hosts.test]
    exec $tclsh \
         [file join $path hosts.tcl] \
         --file $file \
         rm 192.168.1.1 bar
    fileutil::cat $file
} -result "192.168.1.1 foo\n" -cleanup {
    unset file
}
tcltest::test cli-5.1 replace-ip -body {
    set file [tcltest::makeFile {192.168.1.1 foo bar} hosts.test]
    exec $tclsh \
         [file join $path hosts.tcl] \
         --file $file \
         replace-ip bar 192.168.1.2
    fileutil::cat $file
} -result "192.168.1.1 foo\n192.168.1.2 bar\n" -cleanup {
    unset file
}


# Exit with a nonzero status if there are failed tests.
set failed [expr {$tcltest::numTests(Failed) > 0}]

tcltest::cleanupTests
if {$failed} {
    exit 1
}
